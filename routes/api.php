<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::get('/category', function (Request $request) {
        return $request->category();
    });

    Route::apiResource('/categories', CategoryController::class);

});

Route::get('/categories', [CategoryController::class,"index"]);
Route::get('/categories/{category}', [CategoryController::class,"show"]);
Route::post('/signup', [AuthController::class, 'signup']);
Route::post('/signin', [AuthController::class, 'login']);
Route::get('/signin', [AuthController::class, 'login'])->name("signin");
