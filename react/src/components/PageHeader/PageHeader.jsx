import React from 'react';
import "./PageHeader.css"
import {useStateContext} from "../../providers/ContextProvider";
import UserLinksArea from "../UserLinksArea/UserLinksArea";

function PageHeader(props) {

    return (
        <header id={"page-header"}>
            <div>Logo</div>
            <UserLinksArea  />
        </header>
    );
}

export default PageHeader;
