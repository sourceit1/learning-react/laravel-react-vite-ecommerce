import React from 'react';
import "./Pagination.css"

function Pagination(props) {

    return (
        <div className="pagination-block">{
            props.data.links &&
            Object.values(props.data.links).map((item, key) => {
                let text = <span dangerouslySetInnerHTML={{ __html: item.label }}/>
                return <a key={key} className={`${item.active?"active":""}`} onClick={() => {
                    props.handler(item.url)
                }}>{text}</a>
            })
        }
        </div>
    );
}

export default Pagination;
