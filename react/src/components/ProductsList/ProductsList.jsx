import React from 'react';
import "./ProductsList.css"
import {useStateContext} from "../../providers/ContextProvider";
import axiosClient from "../../axios-client";

function ProductsList(props) {

    const {currentCategory, setCurrentCategory} = useStateContext()

    const getProductsList = ($caregoryID) =>{
        axiosClient.get(`/categories/${$caregoryID}`)
            .then(({data}) => {
                setCurrentCategory(data);
            })
            .catch(err => {
                const response = err.response;
                if (response && response.status == 422) {
                    console.log(response.data.message)
                }
            })
    }

    return (
        <div className={"productsList"}>
            <h1>Category: {currentCategory && currentCategory.name}</h1>


        </div>
    );
}

export default ProductsList;
