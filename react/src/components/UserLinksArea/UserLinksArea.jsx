import React from 'react';
import "./UserLinksArea.css"
import {Link, useNavigate} from "react-router-dom";
import {useStateContext} from "../../providers/ContextProvider";

function UserLinksArea(props) {

    const {user, token, onLogout} = useStateContext()
    const navigate = useNavigate()

    return (
        <div>
            <Link to={"/about"}>About</Link>
            {token ? (
                <>
                    <ul className="user-links-container">
                        <li><span>Hello, {user.name}  </span></li>
                        <li><a href="#" onClick={onLogout}>Logout</a></li>
                        <Link to={"/admin"}>Admin Dashboard</Link>
                    </ul>
                </>
            ) : (
                <Link to={"/login"}>Login</Link>
            )

            }

        </div>
    );
}

export default UserLinksArea;
