import "./CategoriesList.css"
import React, {useEffect, useState} from 'react';
import axiosClient from "../../axios-client";
import {useStateContext} from "../../providers/ContextProvider";

function CategoriesList(props) {

    const [categories, setCategories] = useState([]);
    const {currentCategory, setCurrentCategory} = useStateContext()
    useEffect(() => {
        axiosClient.get(`/categories`)
            .then(({data}) => {
                setCategories(data.data);
            })
            .catch(() => {

            })
    }, [])

    const updateCurrentCategory = (e, $caregoryID) =>{
        e.preventDefault();
        axiosClient.get(`/categories/${$caregoryID}`)
            .then(({data}) => {
                setCurrentCategory(data);
            })
            .catch(err => {
                const response = err.response;
                if (response && response.status == 422) {
                    console.log(response.data.message)
                }
            })
    }



    return (
        <div className={"categoriesList"}>
            {
                categories.map((item)=>{
                    let liItem = currentCategory && item.id == currentCategory.id?<b>{item.name}</b>:item.name;
                    return <li onClick={(e)=>{updateCurrentCategory(e, item.id)}} key={item.id}>{liItem}</li>
                })
            }
        </div>
    );
}

export default CategoriesList;
