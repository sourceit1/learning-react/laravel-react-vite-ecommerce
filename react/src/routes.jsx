import React, {lazy} from "react";
import {createBrowserRouter, Navigate} from "react-router-dom";
import Signup from "./pages/Signup/Signup";
import NotFound from "./pages/NotFound";
import DefaultLayout from "./layouts/DefaultLayout";
import GuestLayout from "./layouts/GuestLayout/GuestLayout";
import Dashboard from "./pages/admin/Dashboard/Dashboard";
import Categories from "./pages/admin/Categories/Categories";
import Products from "./pages/admin/Products/Products";
import CategoryForm from "./pages/admin/Categories/CategoriesForm";
import Catalog from "./pages/Catalog/Catalog";
import About from "./pages/About/About";


const Signin = lazy(() => import('./pages/Signin/Signin'));

const router = createBrowserRouter([
    {
        path: '/admin',
        element: <DefaultLayout/>,
        children:[
            {
                path: "/admin/",
                element: <Navigate to="/admin/dashboard" />
            },
            {
                path: '/admin/dashboard',
                element: <Dashboard/>
            },
            {
                path: '/admin/categories',
                element: <Categories/>
            },
            {
                path: '/admin/categories/new',
                element: <CategoryForm key="categoryCreate" />
            },
            {
                path: '/admin/categories/:id',
                element: <CategoryForm key="categoryUpdate" />
            },
            {
                path: '/admin/products',
                element: <Products/>
            },
        ]

    },{
        path: '/',
        element: <GuestLayout/>,
        children: [

            {
                path: '/login',
                element: <Signin/>

            }, {
                path: '/signin',
                element: <Signin/>

            }, {
                path: '/signup',
                element: <Signup/>

            }, {
                path: '/catalog',
                element: <Catalog/>
            }, {
                path: '/about',
                element: <About/>
            }

        ]

    },
    {
        path: '*',
        element: <NotFound/>

    }
])

export default router;
