import {createContext, useContext, useState} from "react";
import axiosClient from "../axios-client";


const StateContext = createContext({
    user: null,
    token: null,
    error: null,
    currentCategory:null,
    setUser: ()=>{},
    setToken: ()=>{},
    setError: ()=>{},
    onLogout: ()=>{},
    setCurrentCategory: ()=>{}

})


export const ContextProvider = ({children}) => {

    let userData = JSON.parse(localStorage.getItem("USER"));
    const [user, _setUser] = useState({
        ...userData
    })
    const [error, setError] = useState({})

    const [token, _setToken] = useState(localStorage.getItem("ACCESS_TOKEN"))

    const [currentCategory, setCurrentCategory] = useState(null);

    const onLogout = (ev) => {
        ev.preventDefault()
        axiosClient.get('/logout')
            .then(() => {
                setUser({})
                setToken(null)
            })
    }

    const setUser = (user)=>{
        _setUser(user)
        if(user){
            localStorage.setItem("USER", JSON.stringify(user));
        }else{
            localStorage.removeItem("USER");
        }
    }
    const setToken = (token)=>{
        _setToken(token)
        if(token){
            localStorage.setItem("ACCESS_TOKEN", token);
        }else{
            localStorage.removeItem("ACCESS_TOKEN");
        }
    }
    return (
        <StateContext.Provider value = {{
            user,
            token,
            error,
            currentCategory,
            setUser,
            setToken,
            setError,
            onLogout,
            setCurrentCategory,
        }}>
            {children}
        </StateContext.Provider>
    )
}

export const useStateContext = ()=> useContext(StateContext);
