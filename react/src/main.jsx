import React, {Suspense} from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import routes from "./routes";
import {RouterProvider} from "react-router-dom"
import {ContextProvider} from "./providers/ContextProvider";


ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <ContextProvider>
            <Suspense fallback={<div>Loading...</div>}>
                <RouterProvider router={routes}></RouterProvider>
            </Suspense>
        </ContextProvider>
    </React.StrictMode>,
)
