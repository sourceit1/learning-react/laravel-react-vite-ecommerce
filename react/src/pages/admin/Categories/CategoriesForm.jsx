import {useNavigate, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import axiosClient from "../../../axios-client.js";


export default function CategoryForm() {
    const navigate = useNavigate();
    let {id} = useParams();
    const [category, setCategory] = useState({
        id: null,
        parent_id: 0,
        name: '',
        description: '',
        image: ''
    })
    const [errors, setErrors] = useState(null)
    const [loading, setLoading] = useState(false)


    if (id) {
        useEffect(() => {
            setLoading(true)
            axiosClient.get(`/categories/${id}`)
                .then(({data}) => {
                    setLoading(false)
                    setCategory(data)
                })
                .catch(() => {
                    setLoading(false)
                })
        }, [])
    }

    const onSubmit = ev => {
        ev.preventDefault()
        if (category.id) {
            axiosClient.put(`/categories/${category.id}`, category)
                .then(() => {

                    navigate('/admin/categories')
                })
                .catch(err => {
                    const response = err.response;

                    if (response && response.status === 422) {
                        setErrors(response.data.errors)
                    }
                })
        } else {
            axiosClient.post('/categories', category)
                .then(() => {

                    navigate('/admin/categories')
                })
                .catch(err => {
                    const response = err.response;
                    if (response && response.status === 422) {
                        setErrors(response.data.errors)
                    }
                })
        }
    }

    return (
        <>
            {category.id && <h1>Редактирование категории: {category.name}</h1>}
            {!category.id && <h1>Новая категория</h1>}
            <div className="card animated fadeInDown">
                {loading && (
                    <div className="text-center">
                        Loading...
                    </div>
                )}
                {errors &&
                <div className="alert">
                    {Object.keys(errors).map(key => (
                        <p key={key}>{errors[key][0]}</p>
                    ))}
                </div>
                }
                {!loading && (
                    <form onSubmit={onSubmit}>
                        <input value={category.name} onChange={ev => setCategory({...category, name: ev.target.value})}
                               placeholder="Name"/>
                        <input value={category.parent_id}
                               onChange={ev => setCategory({...category, parent_id: ev.target.value})}
                               placeholder="Parent ID"/>
                        <textarea value={category.description} name=""
                                  onChange={ev => setCategory({...category, description: ev.target.value})}
                                  placeholder="Description">

                        </textarea>
                        <input value={category.image}
                               onChange={ev => setCategory({...category, image: ev.target.value})} placeholder="image"/>
                        <button className="btn">Save</button>
                    </form>
                )}
            </div>
        </>
    )
}
