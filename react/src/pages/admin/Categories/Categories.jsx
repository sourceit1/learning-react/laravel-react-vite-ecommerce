import {useEffect, useState} from "react";
import axiosClient from "../../../axios-client.js";
import {Link} from "react-router-dom";
import {useStateContext} from "../../../providers/ContextProvider.jsx";
import Pagination from "../../../components/Pagination/Pagination";

export default function Users() {
    const [categories, setCategories] = useState([]);
    const [loading, setLoading] = useState(false);
    const [pagination, setPagination] = useState({});
    const {setNotification} = useStateContext()

    const getCategories = (url = '/categories') => {
        setLoading(true)
        axiosClient.get(url)
            .then(({ data }) => {
                setLoading(false)
                setCategories(data.data)

                setPagination(data.meta)
                //console.log(data.meta)
            })
            .catch(() => {
                setLoading(false)
            })

    }

    useEffect(() => {
        getCategories();
    }, [])

    const onDeleteClick = category => {
        if (!window.confirm("Are you sure you want to delete this user?")) {
            return
        }
        axiosClient.delete(`/categories/${category.id}`)
            .then(() => {
                //setNotification('Category was successfully deleted')
                getCategories();
            })
    }



    return (
        <div>
            <div style={{display: 'flex', justifyContent: "space-between", alignItems: "center"}}>
                <h1>Categories</h1>
                <Link className="btn-add" to="new">Add new</Link>
            </div>
            <div className="card animated fadeInDown">
                <table>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Parent Category</th>
                        <th>Dascription</th>
                        <th>Create Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>
                    {categories.map(c => (
                        <tr key={c.id}>
                            <td>{c.id}</td>
                            <td>{c.name}</td>
                            <td>{c.parent && c.parent.name}</td>
                            <td>{c.description.length > 10 ? c.description.substring(0, 30) + "..." : c.description}</td>
                            <td>{c.created_at}</td>
                            <td>
                                <Link className="btn-edit" to={'./' + c.id}>Edit</Link>
                                &nbsp;
                                <button className="btn-delete" onClick={ev => onDeleteClick(c)}>Delete</button>
                            </td>
                        </tr>
                    ))}
                    </tbody>

                </table>


            </div>
            {
                <Pagination data={pagination} handler={getCategories}/>
            }
        </div>
    )
}
