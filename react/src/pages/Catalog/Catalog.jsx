import React from 'react';
import CategoriesList from "../../components/CategoriesList/CategoriesList";
import ProductsList from "../../components/ProductsList/ProductsList";

function Catalog(props) {
    return (
        <div>
            <CategoriesList></CategoriesList>
            <ProductsList></ProductsList>
        </div>
    );
}

export default Catalog;
