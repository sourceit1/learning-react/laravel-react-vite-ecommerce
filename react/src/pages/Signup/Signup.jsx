import "./Signup.css"
import {createRef, React, useState} from 'react';
import {useStateContext} from "../../providers/ContextProvider";
import {Link,  useNavigate} from "react-router-dom";
import axiosClient from "../../axios-client";


function Signup(props) {

    const nameRef = createRef()
    const emailRef = createRef()
    const passwordRef = createRef()
    const passwordConfirmationRef = createRef()
    let navigate = useNavigate();

    const {setUser, setToken, token} = useStateContext()
    const [errors, setErrors] = useState(null)

    const onSubmit = ev => {
        ev.preventDefault()
        setErrors(null)
        const payload = {
            name: nameRef.current.value,
            email: emailRef.current.value,
            password: passwordRef.current.value,
            password_confirmation: passwordConfirmationRef.current.value,
        }



        axiosClient.post('/signup', payload)
            .then(({data}) => {
                setUser(data.user)
                setToken(data.token);
            })
            .catch(err => {
                const response = err.response;
                if (response && response.status === 422) {
                    setErrors(response.data.errors)
                }
            })
    }

    if(token){
        navigate("/catalog");
    }

    return (


        <div className="container">
            <img src="./rocket.png" alt="rocket" className="rocket" />
            <div className="text">
                <h1>Sign Up </h1>

            </div>
            <form className="form" onSubmit={onSubmit}>
                <div>



                    <input ref={nameRef} type="text"  placeholder="Name"/>
                    {errors && errors.name && <div className="alert">
                        {errors.name[0]}
                    </div>}

                    <input ref={emailRef} type="email" placeholder="Email"/>
                    {errors && errors.email && <div className="alert">
                        {errors.email[0]}
                    </div>}

                    <input ref={passwordRef} type="password" placeholder="Password"/>

                    <input ref={passwordConfirmationRef} type="password" placeholder="Password Confirmation"/>
                    {errors && errors.password && <div className="alert">
                        {errors.password[0]}
                    </div>}
                    <button className="btn" type="submit">SignUp</button>
                    <div>
                        Already have an acount?  <Link to="/login">Sign In</Link>
                    </div>
                </div>

            </form>

        </div>
    );
}

export default Signup;
