import "./Signin.css"
import {createRef, React, useState} from 'react';
import {useStateContext} from "../../providers/ContextProvider";
import {Link, useNavigate} from "react-router-dom";
import axiosClient from "../../axios-client";


function Signin(props) {

    const emailRef = createRef()
    const passwordRef = createRef()
    const {setUser, setToken} = useStateContext()
    const [errors, setErrors] = useState(null)


    const onSubmit = ev => {
        ev.preventDefault()

        const payload = {
            email: emailRef.current.value,
            password: passwordRef.current.value,
        }

        setErrors(null)

        axiosClient.post('/signin', payload)
            .then(({data}) => {
                setUser(data.user)
                setToken(data.token);
            })
            .catch(err => {
                const response = err.response;
                if (response && response.status == 422) {
                    setErrors(response.data.message)
                }
            })


    }


    return (

        <div className="container">
            <img src="./rocket.png" alt="rocket" className="rocket"/>
            <div className="text">
                <h1>Sign In</h1>
            </div>
            <form className="form" onSubmit={onSubmit}>
                <div>
                    {errors && <div className="alert">
                        {errors}
                    </div>
                    }
                    <input ref={emailRef} name="email" type="text" placeholder="Email" autoComplete="email"/>
                    <input ref={passwordRef} name="password" type="password" placeholder="Password"
                           autoComplete="current-password"/>
                </div>
                <div className="check">
                    <p className="forget"><a href="#">Forget Password ?</a></p>
                </div>
                <button className="btn" type="submit">Login</button>
            </form>
            <p className="account">
                <Link to="/signup">Create an account</Link>

            </p>
        </div>
    );
}

export default Signin;
