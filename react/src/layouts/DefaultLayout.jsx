import React from 'react';
import {Link, Navigate, Outlet} from "react-router-dom";
import {useStateContext} from "../providers/ContextProvider";
import axiosClient from "../axios-client";

function DefaultLayout(props) {
    const {user, token, setToken, setUser, onLogout} = useStateContext()




    if(!token){
        return <Navigate to="/signin"/>
    }

    return (
        <div id="defaultLayout">
            <aside>
                <Link to="/admin/dashboard">Dashboard</Link>
                <Link to="/admin/categories">Categories</Link>
                <Link to="/admin/products">Products</Link>
                <a onClick={onLogout} className="btn-logout" href="#">Logout</a>
            </aside>
            <div className="content">
                <header>
                    <div>Header</div>
                    <div>{user.name}</div>
                    <Link to="/catalog">Catalog</Link>
                </header>
                <main>
                    <Outlet/>
                </main>
            </div>
        </div>
    );
}

export default DefaultLayout;
