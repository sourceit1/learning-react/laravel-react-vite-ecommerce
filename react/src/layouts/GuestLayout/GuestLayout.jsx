import React from 'react';
import "./GuestLayout.css"
import {Navigate, Outlet, useLocation} from 'react-router-dom'
import {useStateContext} from "../../providers/ContextProvider";
import PageHeader from "../../components/PageHeader/PageHeader";
import PageFooter from "../../components/PageFooter/PageFooter";

function GuestLayout(props) {
    const location = useLocation();
    const {token} = useStateContext()

    //TODO: Need to redo the redirect condition
    if (token && location.pathname == "/login") {
        return <Navigate to="/catalog"/>
    }
    return (

        <>
            <PageHeader>Header</PageHeader>
            Guest Layout
            <main id={"main"}>
                <Outlet/>
            </main>
            <PageFooter>Footer</PageFooter>
        </>
    );
}

export default GuestLayout;
