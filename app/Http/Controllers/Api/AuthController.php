<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignupRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
/*

@OA\Schema(
 *    schema="UserSchema",
 *    @OA\Property(
 *        property="email",
 *        type="string",
 *        description="User Email",
 *        nullable=false,
 *        example="user2@gmail.com"
    *    ),
 *    @OA\Property(
 *        property="password",
 *        type="string",
 *        description="User Password",
 *        nullable=false,
 *        example="123"
    *    ),
 * )
 */
class AuthController extends Controller
{



    public function signup(SignupRequest $request)
    {
        $data = $request->validated();

        /** @var \App\Models\User $user */
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $token = $user->createToken('main')->plainTextToken;
        return response(compact('user', 'token'));
    }

    /**
     *
     *
     *
     *
     * @OA\POST(
     *     path="/api/signin",
     *     operationId="signin",
     *     tags={"Auth"},
     *     @OA\RequestBody(
     *      @OA\MediaType(
     *       mediaType="application/x-www-form-urlencoded",
     *       @OA\Schema(
     *         required={"email","password"},
     *         @OA\Property(property="email", default="user2@gmail.com"),
     *         @OA\Property(property="password", default="123"),
     *       )
     *     )
     * ),
     *     @OA\Response(
     *         response="422",
     *         description="Invalid input",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="message",
     *                         type="string",
     *                         description="The response error message"
     *                     )
     *                 )
     *             )
     *         }
     *
     *     ),
     *     @OA\Response(
     *     response="200",
     *     description="User signin successful",
     *     content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="message",
     *                         type="string",
     *                         description="The response error message"
     *                     )
     *                 )
     *             )
     *         }
     *
     *
     * )
     * )
     */
    public function login(LoginRequest $request)
    {

        $credentials = $request->validated();
        if (!Auth::attempt($credentials)) {
            return response([
                'message' => 'Provided email or password is incorrect'
            ], 422);
        }

        /** @var \App\Models\User $user */
        $user = Auth::user();
        $token = $user->createToken('main')->plainTextToken;
        return response(compact('user', 'token'));
    }


    /**
     * @OA\Get(
     *     path="/api/logout",
     *     tags={"Auth"},
     *     operationId="logout",
     *      security={{ "sanctum": {} }},
     *    @OA\Response(
     *     response="204",
     *     description="OK",
     *     content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *
     *
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *     response="401",
     *     description="Unauthenticated",
     *     content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     *
     * )
     */
    public function logout(Request $request)
    {
        /** @var \App\Models\User $user */
        $user = $request->user();
        $user->currentAccessToken()->delete();
        return response('', 204);
    }
}
