<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;



class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */

    /**
     * @OA\Get(
     *     path="/api/categories",
     *     tags={"Categories"},
     *     operationId="categoriesList",
     *
     *     @OA\Response(response="200", description="Display a listing of projects.")
     * )
     */
    public function index()
    {
        return CategoryResource::collection(
            Category::query()->orderBy('id')->paginate(10)
        );
    }


    /**
     * Add a new category to the store.
     *
     * @OA\Post(
     *     path="/api/categories",
     *     tags={"Categories"},
     *     operationId="addCategory",
     *     security={{ "sanctum": {} }},
     *     @OA\Response(
     *         response=422,
     *         description="Invalid input"
     *     ),
     *      @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="parent_id",
     *                     description="Parent category ID",
     *                     type="int",
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     description="Category name",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="description",
     *                     description="Category description",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="image",
     *                     description="Path to the image",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     )
     *
     * )
     */
    public function store(StoreCategoryRequest $request)
    {
        $data = $request->validated();
        $category = Category::create($data);

        return response(new CategoryResource($category), 201);
    }

    /**
     * Add a new category to the store.
     *
     * @OA\Get(
     *     path="/api/categories/{categoryId}",
     *     tags={"Categories"},
     *     operationId="getCategory",
     *     security={{ "sanctum": {} }},
     *     @OA\Parameter(
     *         name="categoryId",
     *         in="path",
     *         description="CategoryId",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Invalid input"
     *     ),
     *
     *
     * )
     */
    public function show(Category $category)
    {
        return new CategoryResource($category);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $data = $request->validated();
        $category->update($data);
        return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return response("", 204);
    }
}
