**1)** Clone the repo using SSH. https://monosnap.com/file/oAqwyz4QjE76Qv2c6uESGj22fYBB1Z
   
**git clone git@gitlab.com:sourceit1/learning-react/laravel-react-vite-ecommerce.git**

**2)** Make sure that PHP is installed - "**php -v**"

You should see something like this:

PHP 8.2.5 (cli) (built: Apr 13 2023 18:38:39) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.2.5, Copyright (c) Zend Technologies
with Zend OPcache v8.2.5, Copyright (c), by Zend Technologies 

**3)** Go to the project folder - **cd laravel-react-vite-ecommerce/**

**4)** Now you are in Laravel project folder. Run a command - **composer install**

**5)** Rename **.env.example** to **.env**

**6)** Run a migrations - **php artisan migrate**
You shoult to type **"yes"** in terminal, to the question **"Would you like to create it? (yes/no) [no]"**,
and press **Return**.

**7)** Don`t forget about seeding. **php artisan db:seed**

**8)** Go to the React Project folder - **cd react** 

**9)** Next command is - **npm i**

**10)** Let's launch a project. You should open two terminals or PhpStorm ))
 - In Laravel Project (laravel-react-vite-ecommerce) launch **"php artisan serve"**
 - In React Project (laravel-react-vite-ecommerce/react) launch **"npm run dev"**

**11)** Open [http://localhost:3000](http://localhost:3000)

**Voi-là !!!!!!!!!!!!**
