<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Category>
 */
class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $category = $this->faker->randomElement(Category::all());
        return [
            'parent_id' => $category? $category['id'] : 0,
            'name' => fake()->text(20),
            'description' => fake()->realText(),
            'image' => fake()->imageUrl(640, 480, 'animals', true)
        ];
    }
}
